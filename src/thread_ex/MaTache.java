package thread_ex;

public class MaTache implements Runnable{

	@Override
	public void run() {
		for(int i = 0; i< 10 ;i++) {
			
			String nom = Thread.currentThread().getName();
			System.out.println(nom + " est en cours d'ex�cution");
			try {
				Thread.sleep((int) (Math.random()*200));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public static void main(String [] args) {
		Runnable tache = new MaTache();
		Thread a = new Thread(tache);
		a.setName("Caroline");
		Thread b = new Thread(tache);
		b.setName("Cedric");
		Thread c = new Thread(tache);
		c.setName("Christophe");
		a.start();
		b.start();
		c.start();
		
		try {
			a.join();
			b.join();
			c.join();
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("FIN");
	}
	
}
