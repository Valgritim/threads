package thread_3;

public class Main {

	public static void main(String[] args) {
		Runnable retrait = new JobSylvieEtBruno();
		Thread a = new Thread(retrait);
		a.setName("Sylvie");
		Thread b = new Thread(retrait);
		b.setName("Bruno");
		
		a.start();
		b.start();
	}

}
