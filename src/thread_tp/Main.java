package thread_tp;

public class Main {

	public static void main(String[] args) {
		
		Runnable lettres = new Thread1();
		Runnable chiffres = new Thread2();
		
		new Thread(lettres).start();
		new Thread(chiffres).start();
	}

}
